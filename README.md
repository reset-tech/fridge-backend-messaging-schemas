# Fridge <--> Backend messaging schemas

This repo is intended to maintain all schemas necessary to exchange data 
between the smart fridges and the backend.

The schemas are compliant with the [JSON Schema](http://json-schema.org) 
draft-06.


## Schemas
* [Inventory](schemas/inventory-schema.json)
    - Fridge --> Cloud
* [Status](schemas/status-schema.json)
    - Fridge --> Cloud
* [Commands](schemas/commands-schema.json)
    - Cloud --> Fridge
    
* WIP

## Utilities
- You can check if the json messages created by your application
are compliant with the schema with this [online validator](https://www.jsonschemavalidator.net).
- It is however recommended that you use a validator in your code
for all operations that involve serialization/deserialization of json data.

## Example of json-schema validators
- Java: [json-schema](https://github.com/everit-org/json-schema#when-to-use-this-library)
