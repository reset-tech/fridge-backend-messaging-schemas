# Examples

- inventory-schema message
```json
{
  "fridge_id": 0,
  "user_id": 0,
  "message_timestamp": "2018-11-02T15:25:16.19Z",
  "session_id": 7,
  "id": "b314365d-3505-4b44-aaac-64f1de382467",
  "type": "inventory",
  "message": {
    "removed": [
      {
        "timestamp": "2015-09-28T10:40:00+0200",
        "product_rfid": "EAAB23CD45DF56AB67AF1000"
      }
    ],
    "inserted": [
      {
        "timestamp": "2015-09-28T10:40:00+0200",
        "product_rfid": "12AB23CD45DF56AB67AF1099"
      }
    ],
    "scales": [
      {
        "weight": 100,
        "id": "1"
      },
      {
        "weight": -50,
        "id": "2"
      },
      {
        "weight": 200,
        "id": "3"
      }
    ]
  }
}
```

- status-schema message
```json
{
  "id": "e691ceed-9360-406c-9c84-dd4467d62a1a",
  "message_timestamp": "2015-09-28T10:40:00+0200",
  "session_id": 1,
  "fridge_id": 1234,
  "type": "heartbeat",
  "message": {
    "uptime": 2,
    "fw_version": "23"
  }
}
```

- commands-schema message
```json
{
  "id": "e691ceed-9360-406c-9c84-dd4467d62a1a",
  "message_timestamp": "2015-09-28T10:40:00+0200",
  "session_id": 1,
  "operation": "refill",
  "open_door": true
}
```
